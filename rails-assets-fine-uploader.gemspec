# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rails-assets-fine-uploader/version'

Gem::Specification.new do |spec|
  spec.name          = "rails-assets-fine-uploader"
  spec.version       = RailsAssetsFineUploader::VERSION
  spec.authors       = ["rails-assets.org"]
  spec.description   = "A official bower build for FineUploader/fine-uploader"
  spec.summary       = "A official bower build for FineUploader/fine-uploader"
  spec.homepage      = "https://github.com/FineUploader/bower-dist"
  spec.license       = "https://raw.githubusercontent.com/FineUploader/fine-uploader/mas"

  spec.files         = `find ./* -type f | cut -b 3-`.split($/)
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
